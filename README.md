# McIterators

A barrel containing helper classes for working with iteration in MonkeyC

## Usage

```
import Iterators;

...

class MyIterator extends Iterators.MemoIterator {

    private var _letter = 'A';

    function initialize() {
        Iterator.initialize();
    }

    protected function nextImpl() {
        var result;
        if (_letter <= 'Z') {
            result = letter;
            letter ++;
        } else {
            result = null;
        }
        return result;
    }
}
```

```
var iter = new MyIterator();
while (iter.hasNext()) {
    System.println(iter.next().toString());
}

>A
>B
>C
...
>Z
```

```
var iter = new Iterators.Range(new MyIterator(), 7, 10);
while (iter.hasNext()) {
    System.println(iter.next().toString());
}

>G
>H
>I
```

## License

McIterators is available for use under the MIT license.
