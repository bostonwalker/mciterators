import Toybox.Test;
import Toybox.Lang;
import Toybox.System;


module Iterators {

    class While extends BaseIterator {

        private var _input as BaseIterator;
        private var _condition as Condition;

        private var _endIndex as Number?;

        function initialize(input as BaseIterator, condition as Condition) {
            /*
            An iterator that captures a grouping of items that all meet a condition
            */
            BaseIterator.initialize();

            _input = input;
            _condition = condition;

            _endIndex = null;
        }

        function next() as Object? {
            var result = peek();
            if (result != null) {
                seek ++;
                _input.next();
            }
            return result;
        }

        function peek() as Object? {
            var result;
            if (_endIndex != null && seek == _endIndex) {
                // Reached end
                result = null;
            } else {
                _input.setSeek(seek);
                result = _input.peek();
                if (result == null || !_condition.evaluate(result)) {
                    // Reached end of range
                    _endIndex = seek;
                    result = null;
                }
            }
            return result;
        }

        function setSeek(seek as Number) {
            if (seek == self.seek) {
                // Shortcut
                return;
            } else if (seek < self.seek) {
                self.seek = seek;
                _input.setSeek(seek);
            } else {
                while (self.seek < seek && next() != null) {}
                if (seek == 0 || (seek > 0 && (_endIndex == null || seek <= _endIndex))) {
                    self.seek = seek;
                    _input.setSeek(seek);
                } else {
                    throw new Lang.InvalidValueException(
                        "Item " + seek.toString() + " of While does not exist");
                }
            }
        }

        function size() as Number {
            exploit();
            return _endIndex;
        }

        function toString() as String {
            return "While{" +
                "input=" + _input.toString() + "," +
                "condition=" + _condition.toString() + "}";
        }
    }


    (:test)
    class _Equals extends Condition {

        private var _condition as Object;

        function initialize(condition) {
            Condition.initialize();

            _condition = condition;
        }

        function evaluate(item) as Boolean {
            return 
                (item == null && _condition == null) ||
                (item != null && _condition != null && item.equals(_condition));
        }
    }


    (:test)
    class _LessThan extends Condition {

        private var _threshold;

        function initialize(threshold) {
            Condition.initialize();

            _threshold = threshold;
        }

        function evaluate(item) as Boolean {
            return item != null && item < _threshold;
        }
    }


    (:test)
    function testWhile(logger as Logger) as Boolean {

        var testInput;
        var testCondition;
        var testOutput;
        var obj;
        
        testInput = new ArrayIterator([true, true, false]);
        testCondition = new _Equals(true);
        testOutput = [true, true];
        System.println("Test input: \"" + testInput + "\"");
        obj = new While(testInput, testCondition);
        if (!obj.testAgainstOutput(testOutput)) {
            return false;
        }

        testInput = new ArrayIterator([0, 1, 2, 3, 4]);
        testCondition = new _LessThan(4);
        testOutput = [0, 1, 2, 3];
        System.println("Test input: \"" + testInput + "\"");
        obj = new While(testInput, testCondition);
        if (!obj.testAgainstOutput(testOutput)) {
            return false;
        }
        obj.reset();
        if (!obj.testAgainstOutput(testOutput)) {
            return false;
        }

        return true;
    }


    (:test)
    function testWhilePeekAndHasNext(logger as Logger) as Boolean {

        var testInput = new ArrayIterator([0, 1, 2, 3]);
        var testCondition = new _LessThan(3);
        var obj = new While(testInput, testCondition);

        Test.assertEqual(obj.next(), 0);
        Test.assert(obj.hasNext());

        Test.assertEqual(obj.peek(), 1);
        Test.assertEqual(obj.next(), 1);

        Test.assertEqual(obj.next(), 2);
        Test.assert(!obj.hasNext());
        Test.assert(obj.peek() == null);
        Test.assert(obj.next() == null);

        return true;
    }


    (:test)
    function testWhileGetAndSize(logger as Logger) as Boolean {

        var testInput = new ArrayIterator([0, 1, 2, 3]);
        var testCondition = new _LessThan(3);
        var obj;
        
        obj = new While(testInput, testCondition);
        Test.assertEqual(obj.getSeek(), 0);
        Test.assertEqual(obj.get(0), 0);
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.peek(), 1);
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.get(1), 1);
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.peek(), 2);
        Test.assertEqual(obj.get(2), 2);
        Test.assertEqual(obj.getSeek(), 3);
        Test.assertEqual(obj.size(), 3);

        testInput.reset();
        obj = new While(testInput, testCondition);
        Test.assertEqual(obj.getSeek(), 0);
        Test.assertEqual(obj.get(2), 2);
        Test.assertEqual(obj.getSeek(), 3);
        Test.assert(obj.peek() == null);
        Test.assertEqual(obj.getSeek(), 3);
        Test.assertEqual(obj.get(1), 1);
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.get(0), 0);
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.next(), 1);
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.size(), 3);

        testInput.reset();
        obj = new While(testInput, testCondition);
        Test.assertEqual(obj.size(), 3);
        obj.setSeek(2);
        Test.assertEqual(obj.next(), 2);
        obj.setSeek(0);
        Test.assertEqual(obj.next(), 0);

        return true;
    }


    (:test)
    function testWhileModifiesInputSeek(logger as Logger) as Boolean {

        // First time should modify seek
        var testInput = new ArrayIterator([0, 1, 2, 3, 4, 5]);
        var testCondition = new _LessThan(3);
        var obj = new While(testInput, testCondition);

        obj.exploit();
        Test.assert(testInput.getSeek() == 3);

        // Second time should not
        obj.reset();
        Test.assert(testInput.getSeek() == 3);

        return true;
    }


    (:test)
    function testWhileRange(logger as Logger) as Boolean {

        // Stack While on top of Range
        var testInput = new ArrayIterator([
            "## Contact Report",
            "Proword \"Contact\"",
            "A.  Enemy (G)rid location",
            "B.  (E)nemy description (strength/type)",
            "C.  (O)wn action",
            "D.  (T)ime of contact",
        ]);
        testInput.setSeek(1);
        testInput.peek();
        var testRange1 = new Range(testInput, 1, null);
        testRange1.exploit();
        testInput.peek();
        var testRange2 = new Range(testInput, 2, null);
        testRange2.peek();
        Test.assert(testRange2.next().equals("A.  Enemy (G)rid location"));

        return true;
    }
}
