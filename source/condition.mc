import Toybox.Lang;


module Iterators {

    class Condition {
        // A class with an evaluate() method that returns a boolean
        function evaluate(item as Object?) as Boolean {
            throw new Lang.OperationNotAllowedException(
                "Not implemented");
        }
    }
}
